<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress5' );

/** MySQL database username */
define( 'DB_USER', 'wordpress5' );

/** MySQL database password */
define( 'DB_PASSWORD', '123123123' );

/** MySQL hostname */
define( 'DB_HOST', 'default-mariadb' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'zf-Rt]cjR>2/706#/^`S=hx5>Di9(_>;%{Q64GPy4%N16/]KK}.4z~<?<us|VblT' );
define( 'SECURE_AUTH_KEY',  '36Ijrpzw tItdp=VD_vyKw{6)HTvutU=yn%yj*7uL<$y<Nd.XtFY@w<AaoAUZM2P' );
define( 'LOGGED_IN_KEY',    '.]OhLtZ!hqvD6BS-:PGiHFo[bS Geb>pIX6qo @Ryt_4bqeqlsA;5MQ1Q_gbRcBe' );
define( 'NONCE_KEY',        'kLh*8f/gh29.+WX/smIi (0{yjYQ0u{]>W#Xzl `Qi#;7)=27[/P#mJ=25QoA4go' );
define( 'AUTH_SALT',        'w/397!(F~BY?/d0EwQ9=;6*Tb5nidpshBCP1r!8Ifi}p2UCb]1a$DOQ,D#[-Dn-W' );
define( 'SECURE_AUTH_SALT', 'ua&t_hFQB-)W$t%Em&T(wBd[&;Zrw67IT7=S%)*vG9%/@vvzcrGk]t**<lHyJ2b;' );
define( 'LOGGED_IN_SALT',   '(@@GXfPu-DPZ2!X#C.j*qAAFS6{(I*V2V!P{%.|;_1H3ZW9LL$nU$t~dP%ubl+w0' );
define( 'NONCE_SALT',       'T+wZE9yJ(/Hw<8gPX)2DXBQYz5HO86l>L/TF,<dm2QD_m!gJoz%jvPM:b&-ADQwe' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
